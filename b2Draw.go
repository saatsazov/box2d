package box2d

type B2Draw interface {
	GetFlags() uint
	DrawSolidCircle(center B2Vec2, radius float64, axis B2Vec2, color B2Color)
	DrawSolidPolygon(vertices []B2Vec2, vertexCount int, color B2Color)
	Flush()
	Create()
	DrawCircle(center B2Vec2, radius float64, axis B2Vec2, color B2Color)
	DrawSegment(v1, v2 B2Vec2, color B2Color)
}

const B2Draw_e_shapeBit = 0x0001        ///< draw shapes
const B2Draw_e_jointBit = 0x0002        ///< draw joint connections
const B2Draw_e_aabbBit = 0x0004         ///< draw axis aligned bounding boxes
const B2Draw_e_pairBit = 0x0008         ///< draw broad-phase pairs
const B2Draw_e_centerOfMassBit = 0x0010 ///< draw center of mass frame

type B2Color struct {
	R float64
	G float64
	B float64
	A float64
}

func MakeB2Color(r, g, b float64) B2Color {
	return B2Color{
		R: r,
		G: g,
		B: b,
		A: 1,
	}
}

func MakeB2ColorAlpha(r, g, b, a float64) B2Color {
	return B2Color{
		R: r,
		G: g,
		B: b,
		A: a,
	}
}
